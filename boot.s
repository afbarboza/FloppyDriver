.section	.text
.type		_start, @function
.globl		_start
.code16

_start:
	jmp	main_prog
        .byte   144             #NOP 
        .ascii  "OsandaOS"      #OEMLabel               
        .word   512             #BytesPerSector           
        .byte   1               #SectorsPerCluster        
        .word   1               #ReservedForBoot           
        .byte   2               #NumberOfFats             
        .word   224             #RootDirEntries (224 * 32 = 7168 = 14 sectors to read)          
        .word   2880            #LogicalSectors             
        .byte   0xf0            #MediumByte               
        .word   9               #SectorsPerFat            
        .word   18              #SectorsPerTrack          
        .word   2               #Sides                    
        .long   0               #HiddenSectors              
        .byte   0               #LargeSectors           
        .byte   0               #DriveNo                                   
        .byte   0x29            #Signature (41 for Floppy)
        .long   0x12345678      #VolumeID
        .ascii  "My First OS"   #VolumeLabel
        .ascii  "FAT12   "      #FileSystem */

main_prog:
	cli	
	movw	$str0, %si
	call	printstr

	# reseting the floppy
.reset:	movb	$0x00, %ah
	movb	$0x00, %dl
	int	$0x13
	jc	.reset
	movw	$str1, %si
	call	printstr


	# put the contents of floppy at 0x7E00
	movw	$0x7E0, %ax
	movw	%ax, %es
	xorw	%bx, %bx

	#reading the floppy
.read:	movb	$0x02, %ah	# ler do floppy
	movb	$0x01, %al	# ler um setor, apenas
	movb	$0x00, %ch	# ler da trilha 1
	movb	$0x02, %cl	# setor a ser lido
	movb	$0x00, %dh	# numero da cabeca
	movb	$0x00, %dl	# drive 0 - floppy drive
	int	$0x13
	jc	.read

	movw	$str2, %si
	call	printstr

	movw	$0x7E00, %si
	call	printstr

end:	jmp	.


.include "print.s"

str0:	.ascii "Bootloader v0.1\r\n\0"
str1:	.ascii "Floppy reseted\r\n\0"
str2:	.ascii "Floppy copied\r\n\0"
str3:	.ascii "Not zero"

. = _start + 510
	.byte	0x55, 0xAA

