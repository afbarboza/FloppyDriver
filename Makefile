all:
	as --32 boot.s -o loader.o
	ld -m elf_i386 -nostdlib -N -Ttext 7C00 loader.o -o loader.elf
	objcopy -O binary loader.elf loader.bin
	dd status=noxfer conv=notrunc if=loader.bin of=floppy.flp
	dd if=oi.txt of=floppy.flp seek=1 count=1 bs=512
	qemu-system-i386 -monitor stdio  -fda floppy.flp
